package example

import java.io.FileReader
import java.util.{ Collections, Properties, Optional }

import org.apache.kafka.clients.producer._
import org.apache.kafka.clients.admin.{AdminClient, NewTopic}
import org.apache.kafka.common.errors.TopicExistsException
import scala.util.Random


// TODO
// - set up local kafka (using https://hub.docker.com/r/wurstmeister/kafka)
// - make a proper config file
// - 'scalability' TODOs below


object ClocksProducer extends App {

  // - should get clock ids (or range) from cmd line args or config
  // - jitter config
  // 
  // scalability/threading
  // 1) don't care, thread per clock, kafka client from pool
  //    scale by having multiple processes / nodes
  // 2) ZIO - spin up a million fibers
  // 3) single thread, timer wheel like data structure or just some sorted map?
  //    - insert first 'trigger moment' for each clock
  //    - process all already triggered, insert new trigger moments, sleep until first new trigger moment
  // 4) forkjoin pool executor
  // 5) scala futures

  def buildProperties(configFileName: String): Properties = {
    println(s"building properties for $configFileName")
    val properties: Properties = new Properties

    properties.load(new FileReader(configFileName))
    // TODO this didn't seem to work, hence hard-coding bootstrap.servers for now
    // properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.0.170:9092")
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    properties
  }

  def createTopic(topic: String, clusterConfig: Properties): Unit = {
    val newTopic = new NewTopic(topic, Optional.empty[Integer](), Optional.empty[java.lang.Short]());
    val adminClient = AdminClient.create(clusterConfig)
    try {
      adminClient.createTopics(Collections.singletonList(newTopic)).all.get
    } catch {
      case e :Exception =>
        // Ignore if TopicExistsException, which may be valid if topic exists
        if (!e.getCause.isInstanceOf[TopicExistsException]) {
          throw new RuntimeException(e)
        } else {
          println(s"topic already exists")
        }
    }
    adminClient.close()
  }

  val configFileName = args(0)
  val topicName = args(1)
  val props = buildProperties(configFileName)
  createTopic(topicName, props)

  val producer = new KafkaProducer[String, String](props)

  val clocks = args(2).toInt
  val jitter = args(3).toInt

  val ts = (0 to clocks) map { id =>
    val key = s"clock_$id"
    val t = new Thread {
      override def run(): Unit = {
        def loop(): Unit = {

          println(s"hello $id")

          val value = "" // no need to set any value here? or some timestamp? or does that get assigned by kafka?
          val record = new ProducerRecord[String, String](topicName, key, value)
          producer.send(record) // .get ? can decide to not wait on this

          Thread.sleep(1000 + Random.nextInt(jitter * 2) - jitter)
          loop()
        }

        loop()
      }
    }
    t.run()
    t
  }

  ts foreach { _.join() }
}
