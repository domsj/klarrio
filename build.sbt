import Dependencies._

ThisBuild / scalaVersion     := "2.12.14"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "jan-klarrio",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "org.apache.kafka" % "kafka-clients" % "2.6.2",
    )
  )
